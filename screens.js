/*
 * @param screen JSON {text:"screen name",selected:true,percent:100}
 */
function buildScreenBox(screen) {
    var selectedClass = screen.selected ? "selected" : "";
    return $('<div>')
        .addClass('screen-box')
        .addClass(selectedClass)
        .append($('<div>')
                .addClass('screen-name')
                .text(screen.text)
               );
}

/*
 * @param screens [ {text:"screen name",selected:true,percent:100} ]
 */
function buildScreenRow(screens) {
    var row = $('<div>')
        .addClass('screen-row');
    $.each(screens, function(ndx,val){row.append(buildScreenBox(val));});
    return row;
}

function buildLineRow() {
    return $('<div>')
        .addClass('line-row');
}

function buildLineBox() {
    return $('<div>')
        .addClass('line-box')
        .append($('<div>')
                .addClass('line-box-top'))
        .append($('<div>')
                .addClass('line-box-bottom'));
}

/*
 * @param screens
 {
    rows:
        [
            [
                {
                    text:"screen one",
                    percent:75,
                    selected:true
                },
                {
                    text:"screen two",
                    percent:25,
                    selected:false
                }
            ]
        ]
 }
 */
function buildScreens(screens) {
    $.each(screens.rows, function(ndx,val) {
        $('body').append(buildScreenRow(val));
    });

    $('.screen-row:has(.screen-box.selected)').each(function(ndx,el) {
        var row = $(el);
        var next = row.next('.screen-row');
        var selected = row.find('.screen-box.selected').first();
        if (selected.length === 1 && next.length === 1) {
            var selectedLeft = selected.position().left + selected.outerWidth()/2;
            var selectedTop = selected.position().top + selected.outerHeight();
            var lineRow = buildLineRow();
            next.find('.screen-box').each(function(ndx,el) {
                var screenBox = $(el);
                var screenLeft = screenBox.position().left + screenBox.outerWidth()/2;
                var box = buildLineBox();
                box.width(Math.abs(screenLeft-selectedLeft)+1);
                box.css('top',selectedTop);
                box.css('left',Math.min(screenLeft,selectedLeft));
                var boxTop = box.find('.line-box-top').first();
                var boxBottom = box.find('.line-box-bottom').first();
                if (selectedLeft < screenLeft) {
                    boxTop.css('border-left-style', 'solid');
                    boxBottom.css('border-right-style', 'solid');
                }
                else {
                    boxBottom.css('border-left-style', 'solid');
                    boxTop.css('border-right-style', 'solid');
                }
                lineRow.append(box);
            });
            row.after(lineRow);
        }
    });
}
